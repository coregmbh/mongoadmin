import sys
import os
import pymongo


def initdb(host):

	print "initdb on host", host

	mc = pymongo.MongoClient(host, 8112)

	ehb = mc.EHB

	try:
		allcollections =  ehb.collection_names()
	except pymongo.errors.ServerSelectionTimeoutError:
		print "can't connect to server", host
		exit(1)


	colls = dict(ctelegrams=220000000, clgstelegrams=150000000, cngin=220000000)

	for name, size in colls.items():
		if name in allcollections:
			print "%s is already defined" % name
			continue

		ehb.create_collection(name, capped=True, size=size)


if __name__ == "__main__":

	try:
		host = sys.argv[1]
	except IndexError:
		print "usage: initdb.py host"
		exit(1)

	initdb(host)
	exit(0)


